# docker-sfdx-cli

Dockerfile to create basic image for use with SalesforceDX on Bitbucket Pipelines.

Lightweight Docker image using node alpine, installs SalesforceDX CLI from NPM.

For an example of using this image see this repository: https://bitbucket.org/SalesTrip/pipelines-sample